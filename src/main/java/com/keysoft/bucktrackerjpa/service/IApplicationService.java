package com.keysoft.bucktrackerjpa.service;

import com.keysoft.bucktrackerjpa.entity.Application;

import java.util.List;

public interface IApplicationService {
    List<Application> getAllApplications();
    Application getApplicationById(int applicationId);
    boolean addApplication(Application application);
    void updateApplication(Application application);
    void deleteApplication(int applicationId);
}
